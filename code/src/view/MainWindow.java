package view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import modele.Partie;

import java.io.IOException;

public class MainWindow {
    Partie partie = new Partie();

    @FXML
    private Text curScoreLeft;

    @FXML
    private Text finScoreLeft;

    @FXML
    private Text curScoreRight;

    @FXML
    private Text finScoreRight;

    @FXML
    private TextField nameLeft;

    @FXML
    private TextField nameRight;

    @FXML
    public void scores() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WinnersWindow.fxml"));
        Scene scene = new Scene(loader.load());
        WinnersWindow ww = loader.getController();
        ww.writeWinners(partie);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.UTILITY);
        stage.showAndWait();
    }

    @FXML
    public void launchDice() {
        partie.launchDice();
    }

    @FXML
    public void retainScore() throws IOException {
        partie.saveScore();
        if (partie.end()) {
            openWinWindow();
        }
    }

    private void openWinWindow() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WinWindow.fxml"));
        Scene scene = new Scene(loader.load());
        WinWindow ww = loader.getController();
        ww.writeWinner(partie.getWinner());
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.UTILITY);
        stage.showAndWait();
    }

    @FXML
    public void initialize() {
        nameLeft.textProperty().bindBidirectional(partie.getPlayer1().nameProperty());
        finScoreLeft.textProperty().bind(partie.getPlayer1().finalScoreProperty().asString());
        curScoreLeft.textProperty().bind(partie.getPlayer1().currentScoreProperty().asString());

        nameRight.textProperty().bindBidirectional(partie.getPlayer2().nameProperty());
        finScoreRight.textProperty().bind(partie.getPlayer2().finalScoreProperty().asString());
        curScoreRight.textProperty().bind(partie.getPlayer2().currentScoreProperty().asString());
    }
}

