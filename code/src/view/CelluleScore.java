package view;

import javafx.scene.control.ListCell;
import modele.Score;

public class CelluleScore extends ListCell<Score> {
    @Override
    protected void updateItem(Score score, boolean empty) {
        super.updateItem(score, empty);
        if (!empty) {
            textProperty().bind(score.playerNameProperty());
        } else {
            textProperty().unbind();
            setText("");
        }
    }
}
