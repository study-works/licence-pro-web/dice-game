package view;

import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import modele.Partie;
import modele.Score;

public class WinnersWindow {
    @FXML
    public ListView winnersList;

    @FXML
    public Label pseudo;

    @FXML
    public Label score;

    @FXML
    public void initialize() {
//        winnersList.setCellFactory(new Callback<ListView, CelluleScore>() {
//            @Override
//            public CelluleScore call(ListView listView) {
//                return new CelluleScore();
//            }
//        });

        winnersList.setCellFactory((Callback<ListView, CelluleScore>) listView -> new CelluleScore());

        winnersList.getSelectionModel().selectedItemProperty().addListener((ChangeListener<Score>) (u1, oldValue, newValue) -> {
            if (oldValue != null) {
                pseudo.textProperty().unbind();
                score.textProperty().unbind();
            }
            if (newValue != null) {
                pseudo.textProperty().bind(newValue.playerNameProperty());
                score.textProperty().bind(newValue.scoreProperty().asString());
            }
        });

//        winnersList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Score>() {
//            @Override
//            public void changed(ObservableValue observableValue, Score oldValue, Score newValue) {
//                if (oldValue != null) {
//                    pseudo.textProperty().unbind();
//                    score.textProperty().unbind();
//                }
//                if (newValue != null) {
//                    pseudo.textProperty().bind(newValue.playerNameProperty());
//                    score.textProperty().bind(newValue.scoreProperty().asString());
//                }
//            }
//        });
    }

    public void writeWinners(Partie partie) {
        winnersList.itemsProperty().bind(partie.scoresProperty());
    }
}
