package view;

import javafx.fxml.FXML;
import javafx.scene.text.Text;
import modele.Player;

public class WinWindow {
    @FXML
    private Text winnerText;

    public void close() {
        winnerText.getScene().getWindow().hide();
    }

    public void writeWinner(Player player) {
        winnerText.textProperty().bindBidirectional(player.nameProperty());
    }
}
