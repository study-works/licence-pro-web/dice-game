package modele;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Collections;

public class Partie {
    private final int SCORE_FINAL = 20;

    private ObservableList<Score> scoresObs = FXCollections.observableArrayList();
    private ListProperty<Score> scores = new SimpleListProperty<>(scoresObs);
        public ListProperty<Score> scoresProperty() { return scores; }
        public ObservableList<Score> getScores() { return scores.get(); }
        public void setScores(ObservableList<Score> scores) {this.scores.set( scores); }

    private Dice dice = new Dice();
    private Player player1 = new Player("Joueur 1");
    private Player player2 = new Player("Joueur 2");

    private Player currentPlayer = player1;
    private Player winner;

    public void launchDice() {
        dice.setRandomValue();
        currentPlayer.addScoreCurrent(dice.getValue());
        playerLose();
    }

    public void saveScore() {
        currentPlayer.saveCurrentScore();
        dice.setValue(0);
        this.playerChange();
    }

    public void playerChange() {
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }

    public void playerLose() {
        if (dice.getValue() == 1) {
            currentPlayer.setCurrentScore(0);
            playerChange();
            dice.setValue(0);
        }
    }

    public boolean end() {
        if (player1.getFinalScore() >= SCORE_FINAL) {
            setWinner(player1);
            return true;
        }
        if (player2.getFinalScore() >= SCORE_FINAL) {
            setWinner(player2);
            return true;
        }
        return false;
    }

    private void setWinner(Player player) {
        winner = player;
        scores.add(new Score(winner.getName(), winner.getFinalScore()));
        Collections.sort(scores);
        Collections.reverse(scores);
        player1.setFinalScore(0);
        player2.setFinalScore(0);
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public Player getWinner() {
        return winner;
    }
}
