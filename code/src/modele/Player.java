package modele;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Player {

    public Player(String name) {
        this.name.setValue(name);
    }

    private StringProperty name = new SimpleStringProperty();
        public StringProperty nameProperty() {return name;}
        public String getName() {return name.get();}
        public void setName(String value) {name.set(value);}

    private IntegerProperty finalScore = new SimpleIntegerProperty();
        public IntegerProperty finalScoreProperty() {return finalScore;}
        public Integer getFinalScore() {return finalScore.get();}
        public void setFinalScore(Integer value) {finalScore.set(value);}

    private IntegerProperty currentScore = new SimpleIntegerProperty();
        public IntegerProperty currentScoreProperty() {return currentScore;}
        public int getCurrentScore() {return currentScore.get();}
        public void setCurrentScore(Integer value) {currentScore.set(value);}

    public void addScoreCurrent(int score) {
        currentScore.setValue(currentScore.getValue()+score);
    }

    public void saveCurrentScore() {
        finalScore.setValue(finalScore.get()+currentScore.getValue());
        currentScore.setValue(0);
    }
}
