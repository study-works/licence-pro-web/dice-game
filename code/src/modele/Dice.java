package modele;

import java.util.Random;

public class Dice {
    private int value;

    public void setRandomValue() {
        Random random = new Random();
        value = random.nextInt(6) + 1;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
