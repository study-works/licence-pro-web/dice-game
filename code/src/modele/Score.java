package modele;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Score implements Comparable<Score> {
    private StringProperty playerName = new SimpleStringProperty();
        public String getPlayerName() { return playerName.get(); }
        public StringProperty playerNameProperty() { return playerName; }
        public void setPlayerName(String playerName) { this.playerName.set(playerName); }

    private IntegerProperty score = new SimpleIntegerProperty();
        public int getScore() { return score.get(); }
        public IntegerProperty scoreProperty() { return score; }
        public void setScore(int score) { this.score.set(score); }

    public Score(String playerName, int score) {
        this.playerName.set(playerName);
        this.score.set(score);
    }

    @Override
    public int compareTo(Score s) {
        return Integer.compare(score.get(), s.score.get());
    }
}
